




````
btrfsck  /dev/sda2
Checking filesystem on /dev/sda2
UUID: 75c4d6a3-be91-4ad8-9189-e8c369365c1d
checking extents
checking free space cache
checking fs roots
checking csums
checking root refs
found 5535866880 bytes used err is 0
total csum bytes: 4966324
total tree bytes: 146751488
total fs tree bytes: 132136960
total extent tree bytes: 8355840
btree space waste bytes: 23485296
file data blocks allocated: 5955661824
 referenced 5365964800


Disk /dev/sda: 28.7 GiB, 30765219840 bytes, 60088320 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 90D13B98-2B51-406F-9FC6-02210F397AFF

Device     Start      End  Sectors Size Type
/dev/sda1   2048    18431    16384   8M BIOS boot
/dev/sda2  18432 25165790 25147359  12G Linux filesystem

````



